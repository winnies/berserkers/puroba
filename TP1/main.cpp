// Thomas Galland & Loïc Escales

#include <iostream>
#include <random>
#include <cmath>
#include <cassert>
#include <tuple>

const double PI = 3.141592653589793238463;

// Estimation de pi par la méthode de Monte-Carlo
void monteCarlo() {
    const double circleRadius = 1.0;
    const double squareLenght = 10.0;
    const int nbDraws = 100000;

    std::random_device rd;
    std::mt19937 gen(rd());
    std::uniform_real_distribution<double> dis(-squareLenght / 2.0, squareLenght / 2.0);

    int inTheCircle = 0;
    for (int n = 0; n < nbDraws; ++n) {
        double x = dis(gen);
        double y = dis(gen);

        if (x*x + y*y <= circleRadius * circleRadius) {
            inTheCircle += 1;
        }
    }
    const double pi = std::pow(squareLenght, 2) * (double) inTheCircle / (double) nbDraws;

    std::cout << "Méthode de Buffon : pi = " << pi << std::endl;
}

// Estimation de pi par la méthode de Buffon
void buffonNeedle() {
    const int nbDraws = 1000000;
    const double l = 2.0; // longueur des aiguilles et de l'espace entre deux planches
    const double floorLength = 10000.0;
    assert(floorLength > l);

    std::random_device rd;
    std::mt19937 gen(rd());
    std::uniform_real_distribution<double> floorDis(0.0 + l, floorLength - l);
    std::uniform_real_distribution<double> angleDis(0.0, 10000);

    int nbIntersections = 0;

    for (int n = 0; n < nbDraws; ++n) {
        double x1 = floorDis(gen);
        double y1 = floorDis(gen);
        double angle = angleDis(gen);
        double x2 = x1 + std::cos(angle) * l;
        double y2 = y1 + std::sin(angle) * l;

        if (std::floor(x1 / l) != std::floor(x2 / l)) {
            nbIntersections += 1;
        }
    }
    
    const double p = (double) nbIntersections / (double) nbDraws;
    const double pi = 2.0 / p;
    std::cout << "Méthode de Buffon : pi = " << pi << std::endl;
}

// Estimation de e par deux méthodes différentes
void findE() {
    std::random_device rd;
    std::mt19937 gen(rd());
    std::uniform_real_distribution<double> dis(0.0, 1.0);

    const int N = 1000000;

    double sum_n = 0.0;
    for (int i = 0; i < N; ++i) {
        double sum_r = 0.0;
        int n = 0;

        while (sum_r < 1.0) {
            double r = dis(gen);
            sum_r += r;
            n += 1;
        }

        sum_n +=  n;
    }

    double sum_m = 0.0;
    for (int i = 0; i < N; ++i) {
        int m = 1;

        double r = dis(gen);
        while (true) {
            double r2 = dis(gen);

            m += 1;

            if (r2 < r) {
                break;
            }

            r = r2;
        }

        sum_m +=  m;
    }

    std::cout << "Méthode 1: e = " << sum_n / (double) N << std::endl;
    std::cout << "Méthode 2: e = " << sum_m / (double) N << std::endl;
}

struct Vertex {
    double x = 0, y = 0;
};

struct Vector {
    double x = 0, y = 0;
};

using Polygon = std::vector<Vertex>;

struct Segment {
    Vertex v1, v2;
};

// Permet de créer un polygone manuellement
Polygon getPolygonFromInput() {
    size_t nbVertices;
    std::cout << "Nombre de sommets: ";
    std::cin >> nbVertices;

    std::vector<Vertex> vertices(nbVertices);
    for (size_t i = 0; i < nbVertices; ++i) {
        Vertex v;

        std::cout << "x" << i << ": ";
        std::cin >> v.x;
        std::cout << "y" << i << ": ";
        std::cin >> v.y;

        vertices.push_back(v);
    }

    return vertices;
}

// Retourne un point forcément à l'extérieur du polygone
Vertex getFarVertex(const Polygon& p) {
    Vertex far;

    for (const Vertex& v : p) {
        far.x = std::max(far.x, v.x);
        far.y = std::max(far.y, v.y);
    }

    far.x *= 2;
    far.y *= 2;

    return far;
}

double produit_vectoriel(const Vector& a, const Vector& b) {
    return a.x * b.y - a.y * b.x;
}

Vector vecteur_segment(const Vertex& A, const Vertex& B) {
    return Vector{B.x - A.x, B.y - A.y};
}

template <typename T> int sign(T val) {
    return (T(0) < val) - (val < T(0));
}

// Retourne vrai si les deux segments se croisent
bool collide(const Segment& s1, const Segment& s2) {
    Vertex A(s1.v1), B(s1.v2), C(s2.v1), D(s2.v2);

    Vector AB = vecteur_segment(A, B),
    AC = vecteur_segment(A, C),
    AD = vecteur_segment(A, D),

    CD = vecteur_segment(C, D),
    CA = vecteur_segment(C, A),
    CB = vecteur_segment(C, B);

    double pABAC = produit_vectoriel(AB, AC),
    pABAD = produit_vectoriel(AB, AD),
    pCDCA = produit_vectoriel(CD, CA),
    pCDCB = produit_vectoriel(CD, CB);

    return (sign(pABAC) != sign(pABAD)) and (sign(pCDCA) != sign(pCDCB));
}

// Retourne vrai si le point est à l'intérieur du polygone
bool in(const Vertex& v, const Polygon& p) {
    Segment s1{v, getFarVertex(p)};
    int nbCollide = 0;

    for (size_t i = 0; i < p.size(); ++i) {
        Segment s2{p[i], p[(i+1)%p.size()]};

        if (collide(s1, s2)) {
            nbCollide += 1;
        }
    }

    return nbCollide % 2 != 0;
}

// Retourne les coordonnées minimum d'un nuage de points
Vertex min(const std::vector<Vertex>& vertices) {
    assert(!vertices.empty());

    Vertex r(vertices[0]);
    for (size_t i = 1; i < vertices.size(); ++i) {
        r.x = std::min(r.x, vertices[i].x);
        r.y = std::min(r.y, vertices[i].y);
    }
    return r;
}

// Retourne les coordonnées maximum d'un nuage de points
Vertex max(const std::vector<Vertex>& vertices) {
    assert(!vertices.empty());

    Vertex r(vertices[0]);
    for (size_t i = 1; i < vertices.size(); ++i) {
        r.x = std::max(r.x, vertices[i].x);
        r.y = std::max(r.y, vertices[i].y);
    }
    return r;
}

// Méthode Monte-Carlo (polygone)
double monteCarloPolygon(const Polygon& p) {
    const int nbDraws = 100000;

    Vertex vMin(min(p)), vMax(max(p));

    std::random_device rd;
    std::mt19937 gen(rd());
    std::uniform_real_distribution<double> disX(vMin.x, vMax.x), disY(vMin.y, vMax.y);

    int inTheRectangle = 0;
    for (int n = 0; n < nbDraws; ++n) {
        if (in({disX(gen), disY(gen)}, p)) {
            inTheRectangle += 1;
        }
    }

    return (vMax.x - vMin.x) * (vMax.y - vMin.y) * (double) inTheRectangle / (double) nbDraws;
}

// Méthode Shoelace
double shoelace(const Polygon& p) {
    double sum1 = 0, sum2 = 0;
    for (size_t i = 0; i < p.size(); ++i) {
        sum1 += p[i].x * p[(i+1)%p.size()].y;
        sum2 += p[i].y * p[(i+1)%p.size()].x;
    }
    return std::abs(0.5 * (sum1 - sum2));
}

struct Rectangle {
    Vertex topLeftCorner;
    double width = 0, height = 0;

    Vertex bottomRight() const {
        return Vertex{topLeftCorner.x + width, topLeftCorner.y + height};
    }

    // Retourne vrai si le sommet est contenu dans le rectangle
    bool contains(const Vertex& v) const {
        return v.x > topLeftCorner.x && v.x < topLeftCorner.x + width &&
               v.y > topLeftCorner.y && v.y < topLeftCorner.y + height;
    }
};

std::ostream& operator<<(std::ostream& os, const Rectangle& r) {
    return os << "topLeft: (" << r.topLeftCorner.x << ", " << r.topLeftCorner.y <<"), bottomRight: (" << r.bottomRight().x << ", " << r.bottomRight().y << ")";
}

// Calcule l'erreur lors du PAC learning rectangle
double error_rect(Rectangle hidden, Rectangle guessed) {
    return (hidden.width*hidden.height - guessed.width*guessed.height) / (hidden.width * hidden.height);
}

// PAC learning pour des rectangles axe-parallèle
double PAC_rectangle(double space_size, double precision, double confidence) {
    std::random_device rd;
    std::mt19937 gen(rd());

    // Créé un rectangle aléatoire
    Rectangle hidden_rectangle = [&]() {
        std::uniform_real_distribution<double> dis(0.1*space_size, 0.9*space_size);

        Rectangle rectangle{{dis(gen), dis(gen)}, dis(gen), dis(gen)};

        if(rectangle.topLeftCorner.x + rectangle.width > space_size) {
            rectangle.width = space_size - rectangle.topLeftCorner.x;
        }

        if(rectangle.topLeftCorner.y + rectangle.height > space_size) {
            rectangle.height = space_size - rectangle.topLeftCorner.y;
        }

        return rectangle;
    }();

    std::cout << "Rectangle réel - " << hidden_rectangle << std::endl;

    double m = std::ceil(4/precision * std::log(4/confidence));

    std::cout << "Nombre d'exemples m : " << m << std::endl;

    auto random_vertices = [&]() {
        std::vector<Vertex> vertices;
        vertices.reserve(m);
        std::uniform_real_distribution<double> dis(0,space_size);

        for(int i = 0; i < m; i++) {
            Vertex random_vertex{dis(gen), dis(gen)};

            if(hidden_rectangle.contains(random_vertex)){
                vertices.push_back(random_vertex);
            }
        }

        return vertices;
    }();

    auto guessed_rectangle = [&](){
        Vertex min_v = min(random_vertices);
        Vertex max_v = max(random_vertices);

        return Rectangle {
            min_v,
            max_v.x - min_v.x,
            max_v.y - min_v.y
        };
    }();

    std::cout << "Rectangle déduit - " << guessed_rectangle << std::endl;

    return error_rect(hidden_rectangle, guessed_rectangle);
}

struct Droite {
    double origin = 0, coeff = 0;

    double operator()(double x) const {
        return origin + x * coeff;
    }
};

double norm(const Vertex& v1, const Vertex& v2) {
    return std::sqrt(std::pow(v2.x - v1.x, 2) + std::pow(v2.y - v2.x, 2));
}

std::ostream& operator<<(std::ostream& os, const Droite& d) {
    return os << d.origin << " + x * " << d.coeff;
}

// Tentative plus ou moins ratée d'algorithme d'apprentissage d'un séparateur linéaire maison
// Il aurait peut-être fallu implémenter l'algorithme du perceptron
void PAC_droite(int space_size, double precision, double confidence) {
    double m = std::ceil(3/precision * std::log(3/confidence));
    std::cout << "Nombre d'exemples m : " << m << std::endl;

    std::random_device rd;
    std::mt19937 gen(rd());


    std::uniform_real_distribution<double> disOrigin(0.1*space_size, 0.9*space_size);
    std::uniform_real_distribution<double> disCoeff(-5, 5);
    Droite hidden_droite{disOrigin(gen), disCoeff(gen)};

    std::uniform_real_distribution<double> disSpace(0, space_size);
    std::vector<Vertex> pos_vertices, neg_vertices;
    for (size_t i = 0; i < m; ++i) {
        Vertex random_vertex{disSpace(gen), disSpace(gen)};

        if (hidden_droite(random_vertex.x) < random_vertex.y) {
            neg_vertices.push_back(random_vertex);
        } else {
            pos_vertices.push_back(random_vertex);
        }
    }

    assert(!neg_vertices.empty());
    Vertex petit = neg_vertices[0];
    double petit_norm = norm({0.0, 0.0}, petit);
    for (const Vertex& v : neg_vertices) {
        //if (v.y < petit.y) {
        double n = norm({0.0, 0.0}, v);
        if (n < petit_norm) {
            petit = v;
            petit_norm = n;
        }
    }

    assert(!pos_vertices.empty());
    Vertex grand = pos_vertices[0];
    double grand_norm = norm({0.0, 0.0}, grand);
    for (const Vertex& v : pos_vertices) {
        double n = norm({0.0, 0.0}, v);
        //if (v.y > grand.y) {
        if (n > grand_norm) {
            grand = v;
            grand_norm = n;
        }
    }

    double coeff = (grand.y - petit.y) / (grand.x - petit.x);
    double origin = grand.y - coeff * grand.x;
    Droite guessed_droite{origin, coeff};

    std::cout << "Droite réelle: " << hidden_droite << "; droite estimée: " << guessed_droite << std::endl;
}

int main() {
    std::cout << "Exercice 1" << std::endl;
    monteCarlo();
    buffonNeedle();

    std::cout << std::endl << "Exercice 2" << std::endl;
    findE();

    std::cout << std::endl << "Exercice 3" << std::endl;
    //Polygon p = getPolygonFromInput(); // Décommenter pour entrer manuellement un polygone
    Polygon p{{1, 3}, {3, 5}, {6, 4}, {5, 2}, {2, 1}};
    std::cout << "Aire polygone avec Monte-Carlo: " << monteCarloPolygon(p) << std::endl;
    std::cout << "Aire polygone avec Shoelace: " << shoelace(p) << std::endl;

    std::cout << std::endl << "Exercice 4" << std::endl;
    double erreur = PAC_rectangle(100, 0.001, 0.01);
    std::cout << "Erreur rectangle: " << erreur << std::endl;
    std::cout << "PAC hyperplan" << std::endl;
    PAC_droite(100, 0.001, 0.01);
}
