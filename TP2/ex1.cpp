// Thomas Galland & Loïc Escales

#include <vector>
#include <set>
#include <random>
#include <iostream>

std::random_device rd;
std::mt19937 gen(rd());

using Sommet = size_t;

class Graphe {

public:
    Graphe(size_t ordre) : _adjs(ordre) {}

    void addArc(Sommet i, Sommet j) {
        _adjs[i].insert(j);
    }

    const std::set<Sommet>& adjs(Sommet i) const {
        return _adjs[i];
    }

    size_t ordre() const {
        return _adjs.size();
    }

    static Graphe complet(size_t ordre) {
        Graphe g(ordre);

        for (Sommet s1 = 0; s1 < ordre; ++s1) {
            for (Sommet s2 = 0; s2 < ordre; ++s2) {
                if (s1 == s2) {
                    continue;
                }

                g.addArc(s1, s2);
            }
        }

        return g;
    }

    static Graphe chemin(size_t ordre) {
        Graphe g(ordre);

        for(Sommet s1 = 0; s1 < ordre - 1; ++s1) {
            g.addArc(s1, s1 + 1);
            g.addArc(s1 + 1, s1);
        }
        return g;
    }

    static Graphe sucette(size_t n) {
        Graphe g(2*n);

        for (Sommet s1 = 0; s1 < n; ++s1) {
            for (Sommet s2 = 0; s2 < n; ++s2) {
                if (s1 == s2) {
                    continue;
                }

                g.addArc(s1, s2);
            }
        }
        for(Sommet s1 = n; s1 < 2*n - 1; ++s1) {
            g.addArc(s1, s1 + 1);
            g.addArc(s1 + 1, s1);
        }
        g.addArc(n-1, n);
        g.addArc(n, n-1);
        return g;
    }

private:
    std::vector<std::set<Sommet>> _adjs;
};


struct SommetG {
    size_t row;
    size_t column;

    SommetG(size_t row, size_t column) : row(row), column(column) {}
};

double distance(const SommetG& s1, const SommetG& s2) {
    return std::sqrt(std::pow(s2.row - (float) s1.row, 2) + std::pow(s2.column - (float) s1.column, 2));
}


class Grille {

public:
    Grille(size_t hauteur, size_t largeur): hauteur(hauteur), largeur(largeur){}

    std::vector<SommetG> adjs(SommetG s) const {
        std::vector<SommetG> result;
        if(s.row != 0) result.emplace_back(s.row - 1, s.column);
        if(s.row != hauteur-1) result.emplace_back(s.row + 1, s.column);
        if(s.column != 0) result.emplace_back(s.row, s.column - 1);
        if(s.column != largeur - 1) result.emplace_back(s.row, s.column + 1);
        return result;
    }

private:
    size_t hauteur, largeur;
};


using Path = std::vector<std::pair<Sommet, Sommet>>;

// Marche aléatoire dans un graphe
Path marche_alea(const Graphe& g, Sommet from, Sommet to) {
    Path path;

    while (from != to) {
        const auto& adj = g.adjs(from);
        std::uniform_int_distribution<size_t> dis(0, adj.size() - 1);
        Sommet s = *std::next(adj.begin(), static_cast<long>(dis(gen)));

        path.push_back(std::make_pair(from, s));
        from = s;
    }

    return path;
}

using PathG = std::vector<std::pair<SommetG, SommetG>>;

// Marche aléatoire dans une grille
PathG marche_alea(const Grille& g, SommetG beginning, double distanceMax) {
    PathG path;

    SommetG from = beginning;

    while (true) {
        const auto& adj = g.adjs(from);
        std::uniform_int_distribution<size_t> dis(0, adj.size() - 1);
        SommetG s = *std::next(adj.begin(), static_cast<long>(dis(gen)));

        path.push_back(std::make_pair(from, s));
        if(distance(beginning, s) >= distanceMax){
            break;
        }
        from = s;
    }

    return path;
}

int main() {
    const size_t ordre = 100;
    const int N = 100;

    auto couverture = [&](const Graphe& g, const Sommet& depart) {
        double sum = 0;
        for (size_t i = 0; i < N; ++i) {
            for (Sommet v = 0; v < ordre; ++v) {
                if (depart == v) {
                    continue;
                }
                sum += marche_alea(g, depart, v).size();
            }
        }
        return sum / N;
    };

    auto g = Graphe::complet(ordre);

    double sum = 0;
    for (size_t i = 0; i < N; ++i) {
        sum += marche_alea(g, 0, 1).size();
    }
    std::cout << "Nombre moyen d'étapes pour le premier passage pour une clique d'ordre " << ordre << ": " << sum / N << " (~ ordre)" << std::endl;

    auto couv = couverture(g, 0);
    std::cout << "Nombre moyen d'étapes pour la couverture pour une clique d'ordre " << ordre << ": " << couv << " (~ ordre^2)" << std::endl;

    g = Graphe::chemin(ordre);

    sum = 0;
    for (size_t i = 0; i < N; ++i) {
        auto m = marche_alea(g, 0, ordre - 1);
        sum += m.size();
    }
    std::cout << "Nombre moyen d'étapes pour aller d'une extrémité à l'autre dans un chemin d'ordre " << ordre << ": " << sum / N << " (~ ordre^2)" << std::endl;

    g = Graphe::sucette(ordre);

    couv = couverture(g, 0);
    std::cout << "Nombre moyen d'étapes pour la couverture d'une sucette avec n = " << ordre << " depuis u : " << couv << " (~ ordre^2)" << std::endl;

    couv = couverture(g, ordre*2 - 1);
    std::cout << "Nombre moyen d'étapes pour la couverture d'une sucette avec n = " << ordre << " depuis v : " << couv <<" (~ ordre^3)" << std::endl;

    Grille h(100, 100);
    SommetG depart{50, 50};
    double distanceMax = 30;
    sum = 0;
    for (size_t i = 0; i < N; ++i) {
        auto m = marche_alea(h, depart, distanceMax);
        sum += m.size();
    }
    std::cout << "Nombre moyen d'étapes pour aller vers un sommet distant de " << distanceMax << ": " << sum / N << std::endl;

    return 0;
}
