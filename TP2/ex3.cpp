#include <iostream>
#include <random>
#include <deque>

enum
{
    P = 0,
    F = 1
};

static std::random_device rd;
static std::mt19937 gen(rd());
static std::uniform_int_distribution<int> dis(P, F);

std::pair<int, int> simulateGames(int nb, std::deque<int> player1, std::deque<int> player2)
{
    int player1Count = 0;
    int player2Count = 0;
    for (int i = 0; i < nb; i++)
    {
        std::deque<int> currentDraw;
        for(int j = 0; j < 3; j++)
        {
            currentDraw.push_front(dis(gen));
        }

        while(true)
        {
            if(player1 == currentDraw)
            {
                player1Count ++; break;
            }
            if(player2 == currentDraw)
            {
                player2Count ++; break;
            }
            currentDraw.pop_back();
            currentDraw.push_front(dis(gen));
        }
    }
    return std::make_pair(player1Count, player2Count);
}

int main() {

    std::deque<int> player1 = {P,P,P};
    std::deque<int> player2 = {P,F,P};
    std::pair<int, int> result = simulateGames(500, player1, player2);
    std::cout << "Nb de parties gagnées par j1 : " << result.first << " (~ 2/5), par j2 : " << result.second <<" (~ 3/5)" << std::endl;
}
