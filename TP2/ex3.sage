d = 1/2
q = 1/4
M = Matrix(QQ, [[0, q, q, q, q, 0, 0],
            [0, 0, d, 0, 0, 0, d],
            [0, 0, 0, 0, d, d, 0],
            [0, d, d, 0, 0, 0, 0],
            [0, 0, 0, d, d, 0, 0],
            [0, 0, 0, 0, 0, 1, 0],
            [0, 0, 0, 0, 0, 0, 1]])
Mp= Matrix(QQ, [[0, q, q, q, q, 0, 0],
            [0, 0, d, 0, 0, 0, d],
            [0, 0, 0, 0, d, d, 0],
            [0, d, d, 0, 0, 0, 0],
            [0, 0, 0, d, d, 0, 0],
            [1, 0, 0, 0, 0, 0, 0],
            [1, 0, 0, 0, 0, 0, 0]])
v =  vector(RR, [1, 0, 0, 0, 0, 0, 0])
vpropre = Mp.eigenmatrix_left()
r1 = v * M^500
print("Distribution de M à 500 itérations:")
print(r1)
print("==> Proba que j1 gagne : ", r1[5])
r2 = v * Mp^500
print("Distribution de M' à 500 itérations:")
print(r2)
print("Distribution stationnaire de M' (vecteur propre):")
for vec in vpropre[1]:
    neg = false
    for c in vec:
        if c < 0:
            neg = true
    if neg:
        continue
    
    n = vec.norm(1)
    if n == 0:
        continue
    r3 = vector(RR, vec/vec.norm(1))
    print(r3)
print("==> Proba que j1 gagne : ", r3[5] / (r3[5] + r3[6]))
