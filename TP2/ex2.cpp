#include <iostream>
#include <vector>
#include <random>

std::random_device rd;
std::mt19937 gen(rd());

using Valuation = std::vector<bool>;

struct Litteral {
    size_t var;
    bool neg;

    bool operator()(const Valuation& val) const {
        if (neg) {
            return !val.at(var);
        } else {
            return val.at(var);
        }
    }
};

struct Clause {
    Litteral l1, l2;

    bool operator()(const Valuation& val) const {
        return l1(val) || l2(val);
    }
};

struct Formule {
    std::vector<Clause> clauses;

    bool operator()(const Valuation& val) const {
        for (const Clause& clause : clauses) {
            if (!clause(val)) {
                return false;
            }
        }

        return true;
    }

    static Formule random(size_t varCount, size_t clauseCount) {
        std::uniform_int_distribution<size_t> disVar(0, varCount - 1);
        static std::uniform_int_distribution<size_t> disNeg(0, 1);

        Formule f;

        for (size_t i = 0; i < clauseCount; ++i) {
            f.clauses.push_back({{disVar(gen), (bool) disNeg(gen)}, {disVar(gen), (bool) disNeg(gen)}});
        }

        return f;
    }
};

std::ostream& operator<<(std::ostream& os, const Valuation& val) {
    for (size_t i = 0; i < val.size(); ++i) {
        os << i << " = " << val[i] << "; ";
    }
    return os;
}

std::ostream& operator<<(std::ostream& os, const Litteral& lit) {
    if (lit.neg) {
        os << "¬";
    }
    return os << lit.var;
}

std::ostream& operator<<(std::ostream& os, const Clause& clause) {
    return os << "( " << clause.l1 << " ∨ " << clause.l2 << " )";
}

std::ostream& operator<<(std::ostream& os, const Formule& f) {
    for (size_t i = 0; i < f.clauses.size(); ++i) {
        if (i > 0) {
            os << " ^ ";
        }
        os << f.clauses[i];
    }
    return os;
}

bool resolve(const Formule &f, Valuation& val) {
    const size_t N = val.size() * val.size();
    std::uniform_int_distribution<size_t> disClause(0, f.clauses.size() - 1);
    static std::uniform_int_distribution<size_t> disLit(0, 1);

    for (size_t i = 0; i < N; ++i) {

        size_t beg = disClause(gen);
        size_t j = beg;
        while (f.clauses[j](val)) {
            j = (j+1) % f.clauses.size();
            if (beg == j) {
                std::cout << "Valuation trouvée :\n" << val << "\nNombre d'étapes :\n" << i+1 << std::endl;
                return true;
            }
        }

        const Litteral &l = disLit(gen) == 0 ? f.clauses[j].l1 : f.clauses[j].l2;
        val.at(l.var) = !val.at(l.var);
    }
    std::cout << "Pas de solution trouvée." << std::endl;
    return false;
}

int main() {

    const int N = 1000;

    size_t varCount = 10;
    size_t clauseCount = 20;
    double sum1 = 0;
    for(int i = 0; i < N; i++) {
        std::cout << "--------------------------------------------------" << std::endl;
        Formule f = Formule::random(varCount, clauseCount);
        Valuation val(varCount, false);
        std::cout << f << std::endl;
        if(resolve(f, val)) ++sum1;
    }

    varCount = 20;
    clauseCount = 40;
    double sum2 = 0;
    for(int i = 0; i < N; i++) {
        std::cout << "--------------------------------------------------" << std::endl;
        Formule f = Formule::random(varCount, clauseCount);
        Valuation val(varCount, false);
        std::cout << f << std::endl;
        if(resolve(f, val)) ++sum2;
    }
    std::cout << "--------------------------------------------------" << std::endl;
    std::cout << "Pour 20 clauses et 10 variables: " << (sum1 / N) * 100 << " %." << std::endl;
    std::cout << "Pour 40 clauses et 20 variables: " << (sum2 / N) * 100 << " %." << std::endl;


    return 0;
}
